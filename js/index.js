	$(function(){
		$("[data-toggle = 'tooltip']").tooltip();
		$("[data-toggle = 'popover']").popover();
		$('carousel').carousel({
			interval: 2000
		});

		$('#contacto').on('show.bs.modal', function(e){
			console.log('El modal contacto se está mostrando');
			
			$('#sanAndresBtn').removeClass('btn-outline-primary');
			$('#sanAndresBtn').addClass('btn-primary');
			$('#sanAndresBtn').prop('disabled', true);
								
			$('#santaMartaBtn').removeClass('btn-outline-primary');
			$('#santaMartaBtn').addClass('btn-primary');
			$('#santaMartaBtn').prop('disabled', true);
							
			$('#bogotaBtn').removeClass('btn-outline-primary');
			$('#bogotaBtn').addClass('btn-primary');
			$('#bogotaBtn').prop('disabled', true);

			$('#medellinBtn').removeClass('btn-outline-primary');
			$('#medellinBtn').addClass('btn-primary');
			$('#medellinBtn').prop('disabled', true);

			$('#madridBtn').removeClass('btn-outline-primary');
			$('#madridBtn').addClass('btn-primary');
			$('#madridBtn').prop('disabled', true);	
			});

			$('#contacto').on('shown.bs.modal', function(e){
				console.log('El modal contacto se mostró');
			});

			$('#contacto').on('hide.bs.modal', function(e){
				console.log('El modal contacto se está ocultando');
			});

			$('#contacto').on('hidden.bs.modal', function(e){
				console.log('El modal contacto se ocultó');
			$('#sanAndresBtn').prop('disabled', false);
			$('#sanAndresBtn').removeClass('btn-primary');
			$('#sanAndresBtn').addClass('btn-outline-primary');

			$('#santaMartaBtn').prop('disabled', false);
			$('#santaMartaBtn').removeClass('btn-primary');
			$('#santaMartaBtn').addClass('btn-outline-primary');

			$('#bogotaBtn').prop('disabled', false);
			$('#bogotaBtn').removeClass('btn-primary');
			$('#bogotaBtn').addClass('btn-outline-primary');

			$('#medellinBtn').prop('disabled', false);
			$('#medellinBtn').removeClass('btn-primary');
			$('#medellinBtn').addClass('btn-outline-primary');

			$('#madridBtn').prop('disabled', false);
			$('#madridBtn').removeClass('btn-primary');
			$('#madridBtn').addClass('btn-outline-primary');
		});
	});
